module Statics exposing (..)

import Lang exposing (..)


type alias Env =
    Dict QualifiedName Typ


lookup : Env -> QualifiedName -> Typ
lookup env qname =
    Dict.get qname ? Debug.crash ("Name Error: " ++ toString qname)


typeof_lit : Literal -> Typ
typeof_lit lit =
    case lit of
        String str ->
            TCons ( Just "Basics", "String" ) []

        Char c ->
            TCons ( Just "Basics", "Char" ) []

        Int c ->
            TCons ( Just "Basics", "Int" ) []

        Float c ->
            TCons ( Just "Basics", "Float" ) []


typeof : Env -> Exp -> Typ
typeof env exp =
    case exp of
        Apply dir fun arg ->
            TVar "T"

        Var qname ->
            lookup env qname

        Lit lit ->
            typeof_lit lit

        Tup exps ->
          TTup (exps |> List.map (typeof env))

        Case cases ->
