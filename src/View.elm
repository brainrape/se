module View exposing (view)

import Html exposing (Html, div, span, text, pre, button, table, thead, tbody, th, tr, td, input, label, pre)
import Html.Attributes exposing (class, style, rows, cols, type_, checked, href, rel)
import Html.Events exposing (onInput)
import Html.Events exposing (onClick)
import Maybe.Extra exposing ((?), isJust, isNothing)
import List exposing (map, isEmpty, indexedMap, head, reverse, intersperse)
import Flex exposing (Direction(..))
import Basics.Extra exposing ((=>))
import DictList exposing (DictList, union, singleton, empty, get)
import Lang exposing (..)
import Focus exposing (..)
import Model exposing (..)
import SeStyle exposing (Style)
import SeRender exposing (..)
import OperatorNames exposing (..)
import SeFlex exposing (..)
import StringView exposing (..)
import ViewContext exposing (..)
import Ops exposing (..)


str_exp : Int -> Exp -> String
str_exp =
    StringView.str_exp


preview : Model -> Model
preview model =
    case model.input of
        Just i ->
            let
                unsnaked =
                    if model.opts.snake then
                        unsnake i
                    else
                        i

                input =
                    if unsnaked == "" then
                        "_"
                    else
                        unsnaked
            in
                update_model_string input model

        _ ->
            model


terminal : Context -> Style -> List (Html Msg) -> Html Msg
terminal ctx styl content =
    item (pointer_msgs ctx)
        (border ++ styl ++ pointer_style ctx ++ [ "position" => "relative" ])
    <|
        case ( ctx.cursor, ctx.input ) of
            ( [ FPoint ], Just i ) ->
                content
                    ++ [ div
                            [ style
                                [ "position" => "absolute"
                                , "top" => "-2px"
                                , "left" => "-2px"
                                , "right" => "-2px"
                                , "bottom" => "-1px"
                                ]
                            ]
                            [ view_input i ]
                       ]

            _ ->
                content


container : Context -> Style -> List (Html Msg) -> Html Msg
container ctx styl content =
    ct (border ++ styl ++ cursor_style ctx) content


view : Model -> Html Msg
view model_ =
    let
        model =
            preview model_

        ctx =
            make_context model
    in
        div []
            [ Html.node "style" [] [ text css_ ]
            , Html.node "link" [ href google_fonts, rel "stylesheet" ] []
            , Html.node "script" [] [ text default_key_script ]
              --, div [] [ text (toString model.ast) ]
            , div []
                (if model.opts.source then
                    [ view_source model.src ]
                 else
                    []
                )
            , div []
                [ span [ onClick (OptionsMsg (ShowOptions (not model.opts.showOptions))), style [ "font-size" => "1.2em" ] ] [ text "⚙" ]
                , span []
                    [ if model.opts.showOptions then
                        view_config model.opts |> Html.map OptionsMsg
                      else
                        text ""
                    ]
                ]
            , div [ Html.Events.onMouseLeave (ChangePointer []) ]
                [ view_module ctx model.modul
                ]
            , div []
                [ text
                    (if model.opts.debugKeysPressed then
                        (toString model.keys_pressed)
                     else
                        ""
                    )
                ]
            , div [] <|
                if model.opts.debugCursorPointer then
                    [ div [] [ text (toString model.pointer) ]
                    , div [] [ text (toString model.cursor) ]
                    ]
                else
                    []
            , div []
                (if model.opts.debugInput then
                    [ text <| toString <| model.input ]
                 else
                    []
                )
              --, div [] [ text (toString model.ast) ]
              --, div [] [ text (encode 4 (encode_bindings model.ast.bindings)) ]
              --, pre [] [ text (str_bindings 0 model.modul.bindings) ]
              --, div [ class "module" ] [ view_module model.module_ ]
            ]


view_config : Options -> Html OptionsMsg
view_config opts =
    span [ style [ "font-size" => "0.6em" ] ]
        [ checkbox "source" Source opts.source
        , checkbox "associative function application" AssociativeApply opts.associative_apply
        , checkbox "borders" Borders opts.borders
        , checkbox "alternating" Alternating opts.alternating
        , checkbox "gradient" Gradient opts.gradient
        , checkbox "collapse" CollapseVertical opts.collapse_vertical
        , checkbox "parens" Parens opts.parens
        , checkbox "full names" Qualifiers opts.qualifiers
        , checkbox "infix operators" Infix opts.infix
        , checkbox "snake-case" Snake opts.snake
        , checkbox "direction" Direction opts.direction
        , checkbox "debug cursor and pointer" DebugCursorPointer opts.debugCursorPointer
        , checkbox "debug keys" DebugKeysPressed opts.debugKeysPressed
        , checkbox "debug input" DebugInput opts.debugInput
        ]


view_input : String -> Html Msg
view_input s =
    Html.input
        [ Html.Attributes.id "input"
        , Html.Attributes.value s
        , Html.Attributes.size 5
        , Html.Events.onInput ChangeInput
        , style input_style
        ]
        []


view_module : Context -> Module -> Html Msg
view_module ctx { name, imports, bindings } =
    ct [ "margin-top" => "5px" ]
        [ (bindings |> view_bindings ctx) ]


find_vars : Exp -> List String
find_vars exp =
    []


view_bindings : Context -> Bindings -> Html Msg
view_bindings ctx bindings =
    let
        --locals =
        --    bindings |> DictList.keys |> map (\k -> ( k, () )) |> DictList.fromList
        --ctx =
        --    { ctx_ | locals = DictList.union locals ctx_.locals }
        view_binding_with_typ typ n name exp =
            [ vct binding_style
                [ ct binding_header_style
                    [ terminal (zoom FBindingName (zoom (FBinding name) ctx))
                        binding_name_style
                        [ text (opt_snake ctx.opts name) ]
                    , keyword "\x205F:\x205F"
                    , view_typ (zoom FBindingTyp (zoom (FBinding name) ctx)) typ
                      --, keyword ""
                      --, span [ style (c base02) ] [ text "with " ]
                      --, span [ style (c base03) ] [ text "Basics, List, Html " ]
                    , keyword "\x205F=\x205F"
                    ]
                , ct binding_body_style
                    [ view_exp (zoom FBindingValue (zoom (FBinding name) ctx)) exp ]
                ]
            ]

        view_binding_no_typ n name exp =
            [ ct [] [ text (opt_snake ctx.opts name) ]
            , keyword "\x205F=\x205F"
            , view_exp (zoom FBindingValue (zoom (FBinding name) ctx)) exp
            ]
    in
        vct []
            (bindings
                |> DictList.indexedMap
                    (\n name ( mtyp, exp ) ->
                        vct [ "padding" => "10px" ] <|
                            case mtyp of
                                Just typ ->
                                    view_binding_with_typ typ n name exp

                                Nothing ->
                                    view_binding_no_typ n name exp
                    )
                |> DictList.toList
                |> map Tuple.second
            )


view_typ : Context -> Typ -> Html Msg
view_typ ctx typ =
    case typ of
        TCons qname [] ->
            let
                name_ctx =
                    zoom FTConsName ctx
            in
                item (pointer_msgs name_ctx)
                    (border ++ tcons_name_style ++ pointer_style name_ctx)
                    [ view_qname name_ctx qname ]

        TCons qname args ->
            let
                name_ctx =
                    zoom FTConsName ctx
            in
                ct
                    (tcons_name_style ++ cursor_style ctx)
                    [ lparen ctx.opts
                    , item (pointer_msgs name_ctx)
                        (border ++ pointer_style name_ctx)
                        [ view_qname name_ctx qname
                        ]
                    , text "\x205F"
                    , ct [] (args |> indexedMap (\n t -> t |> view_typ (zoom (FTConsArg n) ctx)))
                    , rparen ctx.opts
                    ]

        TVar var ->
            item (pointer_msgs ctx)
                (border ++ pointer_style ctx ++ tvar_style)
                [ text (opt_snake ctx.opts var) ]

        TArrow t1 t2 ->
            ct
                (border ++ cursor_style ctx ++ pointer_style ctx)
                [ view_typ (zoom FTArrowArg ctx) t1
                , item (pointer_msgs ctx) (pointer_style ctx) [ keyword "\x205F→\x205F" ]
                , view_typ (zoom FTArrowResult ctx) t2
                ]

        TRecord record ->
            ct (pointer_style ctx) [ text (toString record) ]

        TTup typs ->
            (container ctx) []
                [ keyword "("
                , ct []
                    (typs
                        |> indexedMap (\i t -> view_typ (zoom (FTup i) ctx) t)
                        |> (intersperse (keyword "\x205F,\x205F"))
                    )
                , keyword ")"
                ]


view_qname : Context -> QualifiedName -> Html Msg
view_qname ctx ( qs, name ) =
    let
        c_ =
            if head ctx.here == Just FTConsName then
                tcons_name_style
            else
                global_name_style

        ( styl, qualifier ) =
            case ( qs, ctx.opts.qualifiers ) of
                ( Just qname, True ) ->
                    ( c_, ct qualifier_style [ text (qname ++ ".") ] )

                ( Just qname, False ) ->
                    ( c_, text "" )

                ( Nothing, _ ) ->
                    ( (if is_capital name then
                        global_name_style
                       else if ctx.is_pattern then
                        arg_name_style
                       else if ctx.locals |> get name |> isJust then
                        var_style ++ tvar_style
                       else if ctx.modul.bindings |> get name |> isJust then
                        own_name_style
                       else
                        global_name_style
                      )
                    , text ""
                    )
    in
        ct styl
            [ qualifier
            , text (opt_snake ctx.opts name)
            ]


view_exp : Context -> Exp -> Html Msg
view_exp ctx_ exp_ =
    let
        ctx =
            { ctx_ | m_dir = Nothing }
    in
        case exp_ of
            Apply dir fun exp ->
                view_apply ctx_ dir fun exp

            Var qname ->
                terminal ctx
                    []
                    [ view_qname ctx
                        (qname
                            |> Tuple.mapSecond
                                (if ctx.opts.infix then
                                    ligature
                                 else
                                    operator_name
                                )
                        )
                    ]

            Case cases ->
                if ctx.is_pattern then
                    Debug.crash "PATTERN ERROR"
                else
                    view_patmat ctx_ cases

            Lit lit ->
                terminal ctx lit_style [ view_literal ctx lit ]

            Tup t ->
                container ctx
                    ([ "padding" => "4px" ] ++ (vertical_border Horizontal ctx))
                    ([ keyword "(\x205F" ]
                        ++ (t
                                |> indexedMap
                                    (\n exp ->
                                        exp |> view_exp (ctx |> zoom (FTup n)) |> \h -> ct (vertical_border Horizontal ctx) [ h ]
                                    )
                                |> intersperse
                                    (keyword "\x205F,\x205F")
                           )
                        ++ [ keyword "\x205F)" ]
                    )

            Record r ->
                text (toString r)


maybe_borders : Context -> Direction -> a -> a -> a
maybe_borders ctx dir yes no =
    if is_deeper ctx then
        yes
    else
        no


view_apply : Context -> Direction -> Exp -> Exp -> Html Msg
view_apply ctx dir f x =
    let
        maybe_borders_ =
            maybe_borders ctx dir

        render f x =
            (dir_ct_align dir Flex.Start)
                (border ++ vertical_border dir ctx ++ maybe_borders_ (with_border1 dir ctx) [] ++ cursor_style ctx)
                [ (maybe_borders_ (dir_paren dir ctx.opts) (text ""))
                , view_exp (zoom FApplyFun { ctx | m_dir = Just dir, last_vertical_top = dir == Vertical || ctx.last_vertical_top }) f
                , item [ onClick (ChangeDirection (reverse (FPoint :: ctx.here)) (cw dir)) ]
                    []
                    [ dir_direction dir ctx.opts ]
                , ct [] [ view_exp (zoom FApplyArg { ctx | m_dir = Just dir, last_vertical_top = dir == VerticalReverse || ctx.last_vertical_top }) x ]
                , maybe_borders_ (dir_paren (reverse_dir dir) ctx.opts) (text "")
                ]
    in
        case ( ctx.opts.infix, f ) of
            ( True, Apply HorizontalReverse (Var ( qs, fn )) y ) ->
                if not (is_infix fn) then
                    render f x
                else
                    container ctx
                        (border ++ vertical_border dir ctx ++ maybe_borders_ (with_border1 dir ctx) [])
                        [ maybe_borders_ (lparen ctx.opts) (text "")
                        , container (zoom FApplyFun { ctx | m_dir = Just dir })
                            []
                            [ view_exp (zoom FApplyArg (zoom FApplyFun { ctx | m_dir = Just dir })) y
                            , if ctx.opts.direction then
                                rdirection ctx.opts
                              else
                                text "\x205F"
                            , ct [] [ view_exp (zoom FApplyFun (zoom FApplyFun { ctx | m_dir = Just dir })) (Var ( qs, fn )) ]
                            ]
                        , ldirection ctx.opts
                        , ct [] [ view_exp (zoom FApplyArg { ctx | m_dir = Just dir }) x ]
                        , maybe_borders_ (rparen ctx.opts) (text "")
                        ]

            _ ->
                render f x


are_used : DictList String () -> Exp -> Bool
are_used vars exp =
    case exp of
        Apply dir fun arg ->
            (are_used vars fun) || (are_used vars arg)

        Var ( _, name ) ->
            vars |> get name |> isJust

        Tup exps ->
            exps
                |> List.map (are_used vars)
                |> List.any identity

        Case cases ->
            cases
                |> List.map (\( p, r ) -> are_used vars r)
                |> List.any identity

        _ ->
            False


view_patmat : Context -> List ( Exp, Exp ) -> Html Msg
view_patmat ctx cases =
    let
        view_pat n ( pat, res ) =
            ct
                (if n == 0 then
                    []
                 else
                    [ "margin-top" => "4px" ]
                )
                [ view_exp (zoom FCasePattern (zoom (FCase n) { ctx | is_pattern = True })) pat ]

        view_result n ( pat, res ) =
            ct [] <|
                let
                    zoomed =
                        zoom FCaseResult <|
                            zoom (FCase n) <|
                                { ctx
                                    | locals = union ctx.locals (get_pvars pat)
                                }
                in
                    [ view_exp zoomed res ]
    in
        container ctx
            (border
                ++ case_style
                ++ vertical_border Horizontal ctx
                ++ (if
                        not ctx.opts.associative_apply
                            || (ctx.m_dir /= Nothing)
                            || (List.length cases > 1)
                    then
                        with_border1 Horizontal ctx
                    else
                        []
                   )
            )
            --++ [ "border-style" => "dotted" ])
            [ if ctx.opts.parens then
                ct [] [ text "(" ]
              else
                text ""
            , ct [] [ keyword "λ" ]
            , ct []
                [ Html.table [ style table_style ]
                    [ Html.tbody []
                        (cases
                            |> indexedMap
                                (\n ( pat, res ) ->
                                    let
                                        case_ctx =
                                            (zoom (FCase n) ctx)
                                    in
                                        Html.tr
                                            [ style
                                                (border
                                                    ++ pointer_style case_ctx
                                                    ++ cursor_style case_ctx
                                                )
                                            ]
                                            (List.map (Html.td [ Html.Attributes.attribute "valign" "baseline" ])
                                                [ [ view_pat n ( pat, res ) ]
                                                , [ item (pointer_msgs case_ctx) [] [ keyword " ⟼ " ] ]
                                                , [ view_result n ( pat, res ) ]
                                                ]
                                            )
                                )
                        )
                    ]
                ]
            , if ctx.opts.parens then
                ct [] [ text ")" ]
              else
                text ""
            ]


view_literal : Context -> Literal -> Html Msg
view_literal ctx lit =
    text <|
        case lit of
            String s ->
                "“" ++ s ++ "”"

            Char c ->
                "'" ++ toString c ++ "'"

            Int i ->
                toString i

            Float f ->
                toString f
