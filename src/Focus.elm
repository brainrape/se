module Focus exposing (..)

import Maybe.Extra exposing ((?))


type alias Locus =
    List Focus


type Focus
    = FTArrowArg
    | FTArrowResult
    | FTConsName
    | FTConsArg Int
    | FTTup Int
    | FBinding String
    | FBindingName
    | FBindingTyp
    | FBindingValue
    | FApplyFun
    | FApplyArg
    | FCase Int
    | FCasePattern
    | FCaseResult
    | FRecordField Int
    | FRecordFieldName
    | FRecordFieldValue
    | FTup Int
    | FPoint


maybe_fpoint : Locus -> a -> Maybe a
maybe_fpoint locus x =
    case locus of
        FPoint :: [] ->
            Just x

        _ ->
            Nothing


zoom_point : Focus -> Locus -> Locus
zoom_point focus locus =
    (List.head locus
        |> Maybe.Extra.filter ((==) focus)
        |> Maybe.andThen (always (List.tail locus))
    )
        ? []


get_last : Locus -> Maybe Focus
get_last locus =
    case locus of
        focus :: FPoint :: [] ->
            Just focus

        _ :: locus ->
            get_last locus

        [] ->
            Nothing


update_last : Focus -> Locus -> Locus
update_last focus locus =
    let
        update_last focus locus acc =
            case locus of
                _ :: FPoint :: _ ->
                    case focus of
                        FPoint ->
                            acc ++ [ FPoint ]

                        _ ->
                            acc ++ [ focus, FPoint ]

                FPoint :: _ ->
                    (acc ++ [ FPoint ])

                foc :: loc ->
                    update_last focus loc (acc ++ [ foc ])

                [] ->
                    acc ++ [ FPoint ]
    in
        update_last focus locus []


remove_last : Locus -> Locus
remove_last locus =
    let
        remove_last locus acc =
            case locus of
                _ :: FPoint :: _ ->
                    List.reverse (FPoint :: acc)

                focus :: locus ->
                    remove_last locus (focus :: acc)

                [] ->
                    List.reverse (FPoint :: acc)
    in
        remove_last locus []


remove_point : Locus -> Locus
remove_point locus =
    let
        remove_point locus acc =
            case locus of
                FPoint :: _ ->
                    List.reverse acc

                focus :: locus ->
                    remove_point locus (focus :: acc)

                [] ->
                    List.reverse acc
    in
        remove_point locus []
