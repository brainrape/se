module Key exposing (..)

import Keyboard.Extra exposing (Key(..))


modifiers : List Key -> List Key
modifiers keys =
    keys |> List.filter (\k -> k == Control || k == Shift || k == Alt)


plain : Key -> List Key -> Bool
plain key keys =
    List.member key keys && (List.length (modifiers keys) == 0)


ctrl : Key -> List Key -> Bool
ctrl key keys =
    List.member key keys && modifiers keys == [ Control ]


shift : Key -> List Key -> Bool
shift key keys =
    List.member key keys && modifiers keys == [ Shift ]


is_arrow : Key -> Bool
is_arrow key =
    case key of
        ArrowLeft ->
            True

        ArrowRight ->
            True

        ArrowUp ->
            True

        ArrowDown ->
            True

        _ ->
            False


is_alpha : Key -> Bool
is_alpha key =
    let
        code =
            Keyboard.Extra.toCode key
    in
        code >= 65 && code <= 90
