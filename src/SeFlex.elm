module SeFlex exposing (..)

import Html exposing (Html, Attribute, div, span)
import Html.Attributes exposing (style)
import Flex exposing (..)


ct_style : List ( String, String )
ct_style =
    display
        ++ alignItems Surround


dir_ct_style : Direction -> List ( String, String )
dir_ct_style dir =
    ct_style ++ direction dir


dir_ct_align : Direction -> Alignment -> List ( String, String ) -> List (Html msg) -> Html msg
dir_ct_align dir alignment extra_style contents =
    span
        [ style
            (extra_style
                ++ dir_ct_style
                    (if is_reverse dir then
                        reverse_dir dir
                     else
                        dir
                    )
                ++ (if is_vertical dir then
                        alignItems alignment
                    else
                        []
                   )
            )
        ]
        (if is_reverse dir then
            List.reverse contents
         else
            contents
        )


dir_ct : Direction -> List ( String, String ) -> List (Html msg) -> Html msg
dir_ct dir extra_style contents =
    dir_ct_align dir Center extra_style contents


ct : List ( String, String ) -> List (Html msg) -> Html msg
ct extra_style contents =
    span
        [ style (extra_style ++ dir_ct_style Horizontal) ]
        contents


vct : List ( String, String ) -> List (Html msg) -> Html msg
vct extra_style contents =
    span
        [ style (extra_style ++ dir_ct_style Vertical) ]
        contents


item : List (Attribute msg) -> List ( String, String ) -> List (Html msg) -> Html msg
item attrs extra_style contents =
    span
        (style (extra_style ++ ct_style) :: attrs)
        contents


cw : Direction -> Direction
cw dir =
    case dir of
        Horizontal ->
            Vertical

        Vertical ->
            HorizontalReverse

        HorizontalReverse ->
            VerticalReverse

        VerticalReverse ->
            Horizontal


ccw : Direction -> Direction
ccw =
    cw >> cw >> cw


reverse_dir : Direction -> Direction
reverse_dir =
    cw >> cw


is_vertical : Direction -> Bool
is_vertical dir =
    case dir of
        Vertical ->
            True

        VerticalReverse ->
            True

        _ ->
            False


is_reverse : Direction -> Bool
is_reverse dir =
    case dir of
        VerticalReverse ->
            True

        HorizontalReverse ->
            True

        _ ->
            False
