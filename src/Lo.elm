module Main exposing (..)

import Html exposing (..)
import Html.Events exposing (onClick)


type alias Model =
    { a : Int
    , b : Int
    , settingsVisible : Bool
    }


type Msg
    = Increment
    | Decrement


init : Model
init =
    { a = 5
    , b = 6
    , settingsVisible = False
    }


update : Msg -> Model -> Model
update msg model =
    case msg of
        Increment ->
            { model | b = model.b + 1 }

        Decrement ->
            { model | b = model.b - 1 }


view : Model -> Html Msg
view model =
    div []
        [ text (toString model)
        , button [ onClick Increment ] [ text "+" ]
        , button [ onClick Decrement ] [ text "-" ]
        ]


main : Program Never Model Msg
main =
    beginnerProgram { model = init, view = view, update = update }
