module JsonView exposing (..)


encode_exp : Exp -> Json.Encode.Value
encode_exp exp_ =
    case exp_ of
        Apply dir fun arg ->
            object [ ( "Apply", list [ encode_exp fun, encode_exp arg ] ) ]

        Var name ->
            object [ ( "Var", string <| toString name ) ]

        Lit lit ->
            object [ ( "Lit", encode_literal lit ) ]

        Tup exps ->
            object [ ( "Tup", list (List.map encode_exp exps) ) ]

        Case cases ->
            object [ ( "Case", list [ string "it", list (cases |> List.map (\( p, v ) -> list [ encode_exp p, encode_exp v ])) ] ) ]

        Record bindings ->
            object [ ( "Record", encode_bindings bindings ) ]


encode_literal : Literal -> Json.Encode.Value
encode_literal lit =
    case lit of
        String x ->
            string x

        Char x ->
            string (toString x)

        Int x ->
            int x

        Float x ->
            float x


encode_bindings : Bindings -> Json.Encode.Value
encode_bindings bs =
    list (DictList.toList bs |> List.map (\( k, ( t, v ) ) -> list [ string k, encode_exp v ]))
