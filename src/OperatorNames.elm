module OperatorNames exposing (..)


operator_name : String -> String
operator_name op =
    case op of
        "|>" ->
            "flipApply"

        "<|" ->
            "apply"

        "%" ->
            "rem"

        _ ->
            op
