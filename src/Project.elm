module Project exposing (..)

import Set exposing (Set)
import Package exposing (..)
import Lang exposing (..)


type alias Project =
    { package : Package
    , modules : Set Module
    , workspace : Workspace
    }


init =
    { package = initPackage
    , modules = Set.empty
    }
