module Lang exposing (..)

import DictList exposing (DictList)
import Char exposing (isUpper, isLower)
import Maybe.Extra exposing ((?))
import Flex exposing (Direction(..))


type alias Name =
    String


type alias ModuleName =
    String


type alias FieldName =
    String


type alias ConsName =
    String


type alias TypName =
    String


type alias Bindings =
    DictList FieldName ( Maybe Typ, Exp )


type alias Module =
    { name : ModuleName
    , imports : List ( ModuleName, List FieldName )
    , exports : List FieldName
    , types : DictList TypName (DictList ConsName Typ)
    , aliases : DictList TypName Typ
    , bindings : DictList FieldName ( Maybe Typ, Exp )
    }


init_module : Module
init_module =
    { name = "Main"
    , imports = []
    , exports = []
    , types = DictList.empty
    , aliases = DictList.empty
    , bindings = DictList.empty
    }


type Syntax
    = SName String
    | SQualifiedName QualifiedName
    | STyp Typ
    | SExp Exp
    | SBindings Bindings
    | SBinding String (Maybe Typ) Exp
    | SCase Exp Exp


type alias TypDef =
    List ( ConsName, List Typ )


type Typ
    = TCons QualifiedName (List Typ)
    | TArrow Typ Typ
    | TRecord (DictList String Typ)
    | TTup (List Typ)
    | TVar String


type alias QualifiedName =
    ( Maybe String, String )


type Exp
    = Apply Direction Exp Exp
    | Var QualifiedName
    | Lit Literal
    | Tup (List Exp)
    | Case (List ( Exp, Exp ))
    | Record (DictList FieldName Exp)


type Literal
    = String String
    | Char Char
    | Int Int
    | Float Float


is_infix : String -> Bool
is_infix fn =
    (fn
        |> String.uncons
        |> Maybe.map (Tuple.first >> (\c -> not (isUpper c || isLower c)))
    )
        ? False


is_capital : String -> Bool
is_capital fn =
    ((fn |> String.uncons |> Maybe.map (Tuple.first >> (\c -> isUpper c))) ? False)


litToString : Literal -> String
litToString lit =
    case lit of
        String x ->
            x

        Char x ->
            toString x

        Int x ->
            toString x

        Float x ->
            toString x


get_pvars : Exp -> DictList String ()
get_pvars exp =
    case exp of
        Apply dir fun arg ->
            DictList.union (get_pvars fun) (get_pvars arg)

        Var ( Nothing, n ) ->
            if is_capital n then
                DictList.empty
            else
                DictList.singleton n ()

        Tup exps ->
            exps |> List.map (get_pvars >> DictList.toList) |> List.concat |> DictList.fromList

        _ ->
            DictList.empty


exp_dir : Exp -> Maybe Direction
exp_dir exp =
    case exp of
        Apply dir _ _ ->
            Just dir

        Tup _ ->
            Just Horizontal

        Case _ ->
            Just Vertical

        _ ->
            Nothing


syntax_dir : Syntax -> Maybe Direction
syntax_dir syn =
    case syn of
        SExp exp ->
            exp_dir exp

        STyp _ ->
            Just Horizontal

        SBindings _ ->
            Just Vertical

        SBinding _ _ _ ->
            Just Horizontal

        SCase _ _ ->
            Just Horizontal

        _ ->
            Nothing
