module Layout exposing (..)

import Lang exposing (..)


type Layout
    = Horizontal
    | Vertical


type LayoutTree
    = Node Layout Exp (List LayoutTree)
    | Leaf Exp


to_layout_tree : Exp -> LayoutTree
to_layout_tree exp_ =
    case exp_ of
        Apply dir fun arg ->
            Node Horizontal exp_ [ to_layout_tree fun, to_layout_tree arg ]

        Let bindings exp ->
            Node Vertical exp_ [ to_layout_tree exp ]

        Lam arg exp ->
            Node Horizontal exp_ [ Leaf (Var ( Nothing, arg )), to_layout_tree exp ]

        Var name ->
            Leaf exp_

        Lit lit ->
            Leaf exp_

        Tup exps ->
            Node Horizontal exp_ (exps |> List.map (\exp -> to_layout_tree exp))

        Case exp cases ->
            Node Vertical exp_ []

        Record bindings ->
            Node Vertical exp_ []


to_layout : Exp -> Maybe Layout
to_layout exp_ =
    case exp_ of
        Apply dir fun arg ->
            Just Horizontal

        Let bindings exp ->
            Just Vertical

        Lam arg exp ->
            Just Horizontal

        Var name ->
            Nothing

        Lit lit ->
            Nothing

        Tup exps ->
            Just Horizontal

        Case exp cases ->
            Just Vertical

        Record bindings ->
            Just Vertical
