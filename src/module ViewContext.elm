module ViewContext exposing (..)

import Flex exposing (Direction)
import Model exposing (..)
import Focus exposing (..)


type alias Context =
    { opts : Options
    , here : Locus
    , cursor : Locus
    , pointer : Locus
    , is_pattern : Bool
    , m_dir : Maybe Direction
    , locals : Dict String ()
    }


init_ctx : Options -> Locus -> Locus -> Context
init_ctx opts cursor pointer =
    { opts = opts
    , here = []
    , cursor = cursor
    , pointer = pointer
    , is_pattern = False
    , m_dir = Nothing
    , locals = Dict.empty
    }


zoom : Focus -> Context -> Context
zoom focus ctx =
    { ctx
        | here = focus :: ctx.here
        , pointer = zoom_point focus ctx.pointer
        , cursor = zoom_point focus ctx.cursor
    }


is_fun : Context -> Bool
is_fun ctx =
    (List.head ctx.here == Just FApplyFun)


is_arg : Context -> Bool
is_arg ctx =
    (List.head ctx.here == Just FApplyArg)
