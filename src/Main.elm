port module Main exposing (..)

import Html exposing (programWithFlags)
import Keyboard.Extra exposing (Key(..))
import Maybe.Extra exposing (isJust, isNothing)
import Maybe.Extra exposing ((?))
import Key exposing (..)
import Ast
import View exposing (view)
import Model exposing (..)
import Translate exposing (translate)
import Lang exposing (..)
import StringView exposing (..)
import Navigation exposing (..)
import Ops exposing (..)


fbstr : String
fbstr =
    """
main : Html msg
main =
    ul [] (range 1 100 |> map (\\n ->
        li [] [ text (fizzBuzz n) ]))

fizzBuzz : Int -> String
fizzBuzz n =
    case (n % 3, n % 5) of
        (0, 0) -> "fizzbuzz"
        (0, _) -> "fizz"
        (_, 0) -> "buzz"
        _ -> toString n
"""


init : Model
init =
    { init_model
        | src = fbstr
        , modul = translate (Ast.parse fbstr)
    }


str_exp : Int -> Exp -> String
str_exp =
    StringView.str_exp


update_opts : OptionsMsg -> Options -> Options
update_opts msg opts =
    case msg of
        Source x ->
            { opts | source = x }

        AssociativeApply x ->
            { opts | associative_apply = x }

        Borders x ->
            { opts | borders = x }

        Alternating x ->
            { opts | alternating = x }

        Gradient x ->
            { opts | gradient = x }

        CollapseVertical x ->
            { opts | collapse_vertical = x }

        Parens x ->
            { opts | parens = x }

        Qualifiers x ->
            { opts | qualifiers = x }

        Infix x ->
            { opts | infix = x }

        Snake x ->
            { opts | snake = x }

        Direction x ->
            { opts | direction = x }

        DebugKeysPressed x ->
            { opts | debugKeysPressed = x }

        DebugCursorPointer x ->
            { opts | debugCursorPointer = x }

        DebugInput x ->
            { opts | debugInput = x }

        ShowOptions x ->
            { opts | showOptions = x }


key_press : Keyboard.Extra.Key -> Model -> Model
key_press key model =
    if model.input == Nothing && is_arrow key && (modifiers model.keys_pressed == []) then
        movement key model
    else if is_arrow key && (modifiers model.keys_pressed == [ Keyboard.Extra.Shift ]) then
        { model
            | cursor =
                select_parent model.cursor model.modul.bindings
        }
        --else if is_alpha key then
        --    { model
        --        | input =
        --            model.input
        --                |> Maybe.map
        --                    (\i ->
        --                        i ++ String.fromChar (Char.fromCode (Keyboard.Extra.toCode key))
        --                    )
        --    }
    else if key == Enter && (modifiers model.keys_pressed == [ Keyboard.Extra.Control ]) then
        if isNothing model.input then
            new_binding model
        else
            model
    else
        case key of
            Enter ->
                case model.input of
                    Just i ->
                        accept_input i model

                    Nothing ->
                        start_input model

            --BackSpace ->
            --    { model
            --        | input =
            --            model.input
            --                |> (Maybe.andThen (\i -> i |> String.toList |> List.Extra.init |> Maybe.map String.fromList))
            --    }
            --Space ->
            --    { model | input = Nothing }
            _ ->
                model


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Nop ->
            ( model, Cmd.none )

        ChangeSrc src ->
            ( { model
                | modul =
                    translate (Ast.parse src)
                    --, src = src
              }
            , Cmd.none
            )

        OptionsMsg optsmsg ->
            ( { model | opts = update_opts optsmsg model.opts }, Cmd.none )

        ChangePointer pointer ->
            ( { model | pointer = pointer }, Cmd.none )

        ChangeCursor cursor ->
            ( { model | cursor = cursor, pointer = [] }, Cmd.none )

        ChangeDirection point dir ->
            let
                modul =
                    model.modul |> \modul -> { modul | bindings = update_bindings_dir dir model.modul.bindings point ? modul.bindings }
            in
                ( { model | modul = modul }, Cmd.none )

        KeyDown key ->
            let
                new_model =
                    key_press key model

                cmd =
                    if isNothing model.input && isJust new_model.input then
                        Cmd.batch
                            [ select ( 0, 9999999 )
                              --, Task.attempt (always Nop) (Dom.focus "input")
                            ]
                    else
                        Cmd.none
            in
                ( new_model, cmd )

        --( { new_model | src = str_bindings 0 new_model.modul.bindings }, cmd )
        KeyMsg msg ->
            ( { model | keys_pressed = Keyboard.Extra.update msg model.keys_pressed }, Cmd.none )

        ChangeInput str ->
            ( { model | input = Just str }, Cmd.none )


port select : ( Int, Int ) -> Cmd msg


main : Program {} Model Msg
main =
    programWithFlags
        { init = \_ -> ( init, Cmd.none )
        , update = update
        , view = view
        , subscriptions =
            \_ ->
                Sub.batch
                    [ Keyboard.Extra.downs KeyDown
                    , Keyboard.Extra.subscriptions |> Sub.map KeyMsg
                    ]
        }
